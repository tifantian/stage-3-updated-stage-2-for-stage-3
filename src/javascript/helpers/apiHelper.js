const API_URL = 'https://bsa19-stage3-nodejs.herokuapp.com';

function callApi(endpoind, method) {
  const url = API_URL + endpoind;
  console.log(url);
  const options = {
    method
  };

  return fetch(url, options)
    .then(response =>
      response.ok ? response.json() : Promise.reject(Error('Failed to load'))
    )
    .catch(error => {
      throw error;
    });
}

export { callApi }
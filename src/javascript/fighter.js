class Fighter {
    constructor(fighterDetails) {
        this.name = fighterDetails.name;
        this.health = fighterDetails.health;
        this.attack = fighterDetails.attack;
        this.defense = fighterDetails.defense;
        this.img = fighterDetails.source;
    }

    getHitPower() {
        const criticalHitChance = Math.random() + 1;
        return this.attack * criticalHitChance;
    }

    getBlockPower() {
        const dodgeChance = Math.random() + 1;
        return this.defense * dodgeChance;
    }
}

export default Fighter;
import View from "./view";
import App from "./app";
import Fighter from "./fighter";
import Fight from "./fight";


class StartButton extends View {
    constructor() {
        super();
        this.createStartButton();
    }

    setFighters(first, second) {
        this.firstFighter = first;
        this.secondFighter = second;
    }

    createStartButton() {
        this.element = this.createElement({
            tagName: 'div',
            className: 'button-start'
        });
        this.element.innerText = 'FIGHT';
        this.element.style.visibility = 'hidden';
        this.element.addEventListener('click', () => { 
            const fighter1 = new Fighter(this.firstFighter);
            const fighter2 = new Fighter(this.secondFighter);
            App.rootElement.style.display = 'none';
            new Fight(fighter1, fighter2);
         });
        App.rootElement.append(this.element);
    }
}

export default StartButton;
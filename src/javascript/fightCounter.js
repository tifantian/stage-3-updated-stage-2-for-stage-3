class FightCounter {
    constructor(allHpFighter1, allHpfighter2) {
        this.allHpFighter1 = allHpFighter1;
        this.allHpFighter2 = allHpfighter2;

        const healtPoints = document.getElementsByClassName('info-tablo')[0];
        const damageTablo = document.getElementsByClassName('damage-tablo')[0];
        this.countFight = document.getElementsByClassName('count-fighing')[0];
        this.resultOfGame = document.getElementsByClassName('result-of-game')[0];
        this.winnerGame = document.getElementsByClassName('winner-name')[0];
        this.health1 = healtPoints.firstElementChild;
        this.health2 = healtPoints.lastElementChild;
        this.damage2 = damageTablo.firstElementChild;
        this.damage1 = damageTablo.lastElementChild;
    }

    changeHealthFirst(health) {
        if( (health / this.allHpFighter1) <= 0.5 && (health / this.allHpFighter1) > 0.15) {
            this.health1.style.color = 'yellow';
        } else if ( (health / this.allHpFighter1) <= 0.15 ) {
            this.health1.style.color = 'red';
        }
        this.health1.innerText = health.toFixed(2);
    }

    changeHealthSecond(health) {
        if( (health / this.allHpFighter2) <= 0.5 && (health / this.allHpFighter2) > 0.15) {
            this.health2.style.color = 'yellow';
        } else if( (health / this.allHpFighter2) <= 0.15 ) {
            this.health2.style.color = 'red';
        }
        this.health2.innerText = health.toFixed(2);
    }

    changeDamageFirst(damage) {
        if(damage == 0) {
            this.damage1.style.color = 'green';
            this.damage1.innerText = "Block";
        } else {
            this.damage1.style.color = '';
            this.damage1.innerText = `-${damage.toFixed(2)}`;
        }
    }

    changeDamageSecond(damage) {
        if(damage == 0) {
            this.damage2.style.color = 'green';
            this.damage2.innerText = "Block";
        } else {
            this.damage2.style.color = '';
            this.damage2.innerText = `-${damage.toFixed(2)}`;
        }
    }

    changeFightCount(count) {
        const word = (count > 1) ? "hits": "hit";
        this.countFight.innerText = `${count} ${word}`;
    }

    showResult(fighter1, fighter2) {
        this.resultOfGame.style.color = 'green';
        if(fighter1.health <= 0 && fighter2.health <= 0) {
            this.resultOfGame.style.color = 'yellow';
            this.resultOfGame.innerText = 'DRAW';
        } else if(fighter1.health <= 0) {
            this.resultOfGame.innerText = 'WIN';
            this.winnerGame.innerText = fighter2.name;
        } else if(fighter2.health <= 0) {
            this.resultOfGame.innerText = 'WIN';
            this.winnerGame.innerHTML = fighter1.name;
        }

    }
}


export default FightCounter;
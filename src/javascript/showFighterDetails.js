import View from './view'
import App from './app';
import FightersView from './fightersView';

class showFighterDetails extends View {
    constructor(obj) {
        super();
        this.details = {
            name: obj.name, 
            health: obj.health, 
            attack: obj.attack, 
            defense: obj.defense
        };
        this._id = obj._id;
        this.element = this.createModal();
    }

    createModal() {
        const modal = this.createElement({tagName: 'div', className: 'show-details-modal'}); // boss
        const buttonExit = this.createElement({tagName: 'div', className: 'bottom-exit'});
        buttonExit.innerText = 'Exit';
        const buttonChangeInfo = this.createElement({tagName: 'div', className: 'change-info'});
        buttonChangeInfo.innerText = 'Change info';
        buttonChangeInfo.addEventListener('click', ()=> {
            this.changeInfo();
            App.rootElement.lastElementChild.remove();
            App.rootElement.lastElementChild.style.display = ''; 
        });

        buttonExit.addEventListener('click', () => {
           App.rootElement.lastElementChild.remove();
           App.rootElement.lastElementChild.style.display = ''; 
        });
        const windowDetails = this.createElement({tagName: 'div', className: 'window-details'});
        this.getDetailsBlock(windowDetails);
        
        modal.append(buttonExit, windowDetails, buttonChangeInfo);
        
        return modal;
    }

    getDetailsBlock(windowDetails) {
        for(const key in this.details) {
            const input = this.createElement({
                tagName: 'input',
                className: 'input-details',
                attributes: {'value': `${this.details[key]}`}
            });
            const description = this.createElement({
                tagName: 'div',
                className: 'description-details'
            });
            description.innerText = `${key}`;
            windowDetails.append(description, input);
        }
    }

    changeInfo() {
        const newObjectDetails = FightersView.fightersDetailsMap.get(this._id);
        const modal = document.getElementsByClassName('show-details-modal')[0];
        const inputs = modal.getElementsByTagName('input');
        const changes = {
            name: inputs[0].value,
            health: inputs[1].value,
            attack: inputs[2].value,
            defense: inputs[3].value
        }
        App.rootElement.children[1].children[this._id - 1].children[1].innerText = changes.name; // Прошу прощения за этот стыд...
        Object.assign(newObjectDetails, changes);
        FightersView.fightersDetailsMap.set(this._id, newObjectDetails);
    }
}

export default showFighterDetails;
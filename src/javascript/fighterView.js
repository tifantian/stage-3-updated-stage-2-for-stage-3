import View from './view';

class FighterView extends View {
  constructor(fighter, handleClick, clickCheckBox) {
    super();

    this.createFighter(fighter, handleClick, clickCheckBox);
  }

  createFighter(fighter, handleClick, clickCheckBox) {
    const { name, source } = fighter;
    const nameElement = this.createName(name);
    const imageElement = this.createImage(source);
    const checkBoxElement = this.createCheckBox(fighter, clickCheckBox);
    this.element = this.createElement({ tagName: 'div', className: 'fighter' });
    this.element.append(imageElement, nameElement, checkBoxElement);
    this.element.addEventListener('click', event => handleClick(event, fighter), false);
  }

  createName(name) {
    const nameElement = this.createElement({ tagName: 'span', className: 'name' });
    nameElement.innerText = name;

    return nameElement;
  }

  createImage(source) {
    const attributes = { src: source };
    const imgElement = this.createElement({
      tagName: 'img',
      className: 'fighter-image',
      attributes
    });

    return imgElement;
  }

  createCheckBox(fighter, clickCheckBox) {
    const checkbox = this.createElement({
      tagName: 'input',
      className: 'input-checkbox',
      attributes: {'type': 'checkbox'}
    });
    checkbox.addEventListener('click', event => clickCheckBox(event, fighter), false);

    return checkbox;
  }
}

export default FighterView;
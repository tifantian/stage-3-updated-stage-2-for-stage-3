import View from './view';
import FighterView from './fighterView';
import { fighterService } from './services/fightersService';
import showFighterDetails from './showFighterDetails';
import App from './app';
import StartButton from './startButton';



class FightersView extends View {
  constructor(fighters) {
    super();
    this.handleClick = this.handleFighterClick.bind(this);
    this.clickCheckBox = this.handleCheckBoxClick.bind(this);
    this.createFighters(fighters);
  }

  static fightersDetailsMap = new Map();
  selectedFighters = [];
  buttonView = new StartButton();

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick, this.clickCheckBox);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
  }

  async handleFighterClick(event, fighter) {
    if(event.target.classList.contains('input-checkbox')) {
      return;
    }
    if(!FightersView.fightersDetailsMap.has(fighter._id)) {
      const fighterDetails = await fighterService.getFighterDetails(fighter._id);
      FightersView.fightersDetailsMap.set(fighter._id, fighterDetails);
    }
    const instanseFighter = new showFighterDetails(FightersView.fightersDetailsMap.get(fighter._id));
    const modalElement = instanseFighter.element;
    App.rootElement.lastElementChild.style.display = 'none';
    App.rootElement.append(modalElement);
  }

  async handleCheckBoxClick(event, fighter) {
    if(!FightersView.fightersDetailsMap.has(fighter._id)) {
      const fighterDetails = await fighterService.getFighterDetails(fighter._id);
      FightersView.fightersDetailsMap.set(fighter._id, fighterDetails);
    }
    this.buttonView.element.style.visibility = 'hidden';
    if(event.target.checked && this.selectedFighters.indexOf(fighter._id) == -1) {
      this.selectedFighters.push(fighter._id);
    } else {
      const index = this.selectedFighters.indexOf(fighter._id);
      this.selectedFighters.splice(index, 1);
    }
    if(this.selectedFighters.length == 2) {
      this.buttonView.element.style.visibility = 'visible';

      const [firstIndex, secondIndex] = this.selectedFighters;
      
      this.buttonView.setFighters(
        FightersView.fightersDetailsMap.get(firstIndex),
        FightersView.fightersDetailsMap.get(secondIndex)
      );
    } else {
      // Показать что чтобы начать надо 2 файтера
    }
    
  }

}

export default FightersView;
import View from "./view";
import App from "./app";

class FieldView extends View {
    constructor(fighter1, fighter2) {
        super();
        
        this.createWindow(fighter1, 'left');
        this.createScoreboard(fighter1, fighter2);
        this.createWindow(fighter2, 'right');
        this.createButtonExit();
    }

    static clearTimer = false;

    createButtonExit() {
        this.element = this.createElement({
            tagName: 'div',
            className: 'field-exit-button'
        });

        this.element.addEventListener('click', function() {
            App.fightField.style.display = 'none';
            while (App.fightField.firstChild) {
                App.fightField.removeChild(App.fightField.firstChild);
            }
            FieldView.clearTimer = true;
            App.rootElement.style.display = '';
        });

        App.fightField.append(this.element);
    }

    createScoreboard(fighter1, fighter2) {
        this.element = this.createElement({
            tagName: 'div',
            className: 'field-score-board'
        });
        const textFight = this.createElement({
            tagName: 'span',
            className: 'field-text-fight'
        })
        textFight.innerText = 'Players fight each other at the same time';
        const infoTablo = this.createInfoTablo(fighter1.health, fighter2.health);
        const damageTablo = this.createDamageTablo();
        const countFighing = this.createElement({tagName: 'div', className: 'count-fighing'});
        countFighing.innerText = '0 hit';
        const result = this.createResultWindow();
        this.element.append(textFight, countFighing, infoTablo, damageTablo, result);
        App.fightField.append(this.element);
    }

    createInfoTablo(hp1, hp2) {
        const infoTablo = this.createElement({
            tagName: 'div',
            className: 'info-tablo'
        });
        const health1 = this.createElement({
            tagName: 'span',
            className: 'helath-fighter-1'
        });
        health1.innerText = hp1;
        const health2 = this.createElement({
            tagName: 'span',
            className: 'helath-fighter-2'
        });
        health2.innerText = hp2;
        infoTablo.append(health1, health2);
        return infoTablo;
    }

    createWindow(fighter, position) {
        const {name, attack, defense, health, img} = fighter;

        const nameElement = this.createName(name);
        const imageElement = this.createImage(img, position);
        const infoElement = this.createInfo(attack, defense);
        const healthElement = this.createHealth(health);

        this.element = this.createElement({ tagName: 'div', className: 'field-fighter' });
        this.element.append(nameElement, imageElement, infoElement, healthElement);
        App.fightField.append(this.element);
    }

    createName(name) {
        const nameElement = this.createElement({
            tagName: 'span',
            className: 'field-fighter-name'
        });
        nameElement.innerText = name;
        return nameElement;
    }  

    createImage(img, position) {
        let className = `field-fighter-img`;
        if(position == 'right') {
            className += `-${position}`;
        }
        const imageElement = this.createElement({
            tagName: 'img',
            className,
            attributes: {'src': img}
        });
        return imageElement;
    }

    createInfo(attack, defense) {
        const container = this.createElement({
            tagName: 'div',
            className: 'field-info'
        });
        const span1 = this.createElement({
            tagName: 'span',
            className: 'field-attack'
        });
        span1.innerHTML = `attack: ${attack}<br>`;
        const span2 = this.createElement({
            tagName: 'span',
            className: 'feld-defense'
        });
        span2.innerText = `defense: ${defense}`;
        container.append(span1, span2);
        return container;
    }

    createHealth(hp) {
        const healthElement = this.createElement({
            tagName: 'div',
            className: 'field-fighter-health'
        });
        healthElement.innerText = `health: ${hp}`;
        return healthElement;
    }

    createDamageTablo() {
        const damageTablo = this.createElement({
            tagName: 'div',
            className: 'damage-tablo'
        });
        const damage1 = this.createElement({
            tagName: 'span',
            className: 'damage-fighter-1'
        });
        damage1.innerText = '';
        const damage2 = this.createElement({
            tagName: 'span',
            className: 'damage-fighter-2'
        });
        damage2.innerText = '';
        damageTablo.append(damage1, damage2);
        return damageTablo;
    }

    createResultWindow() {
        const winnerWindow = this.createElement({
            tagName: 'div',
            className: 'show-winner'
        });
        const result = this.createElement({
            tagName: 'span',
            className: 'result-of-game'
        });
        const nameWinner = this.createElement({
            tagName: 'span',
            className: 'winner-name'
        });
        winnerWindow.append(nameWinner, result);
        return winnerWindow;
    }

}

export default FieldView;
import App from "./app";
import FieldView from './fieldView';
import FightCounter from './fightCounter';

class Fight {
    constructor(p1, p2) { // player 1, player 2
        this.p1 = p1;
        this.p2 = p2;
        this.createField(p1, p2);
        this.startFight();
    }

    static counter = 0;

    createField(fighter1, fighter2) {
        App.fightField.style.display = '';
        this.field = new FieldView(fighter1, fighter2);
    }

    startFight() {
        Fight.counter = 0;
        const instanseFight = new FightCounter(this.p1.health, this.p2.health);
        let endOfGame = false;
        FieldView.clearTimer = false;
        const timerId = setInterval(() => {
            if(FieldView.clearTimer) {
                clearInterval(timerId);
            }
            Fight.counter++;
            instanseFight.changeFightCount(Fight.counter);
            let damage = this.makeOneTurn(this.p1, this.p2);
            instanseFight.changeDamageFirst(damage);
            instanseFight.changeHealthSecond(this.p2.health);
            if(this.p2.health <= 0) {
                clearInterval(timerId);
                endOfGame = true;
            }
            damage = this.makeOneTurn(this.p2, this.p1);
            instanseFight.changeDamageSecond(damage);
            instanseFight.changeHealthFirst(this.p1.health);
            
            if(this.p1.health <= 0) {
                clearInterval(timerId);
                endOfGame = true;
            }
            if(endOfGame) {
                instanseFight.showResult(this.p1, this.p2);
            } 
        }, 1000);
    }

    makeOneTurn(attacking, defender) {
        let change = attacking.getHitPower() - defender.getBlockPower();
        if(change <= 0) {
            change = 0;
        } else {
            defender.health -= change;
        }
        return change;
    }

}

export default Fight;